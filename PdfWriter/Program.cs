﻿using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PdfWriter
{
    class Program
    {
        static void Main(string[] args)
        {
            string filename = "C:/Users/ECJ4REAL.ECJ4REAL-PC/Desktop/mypdffile.pdf";
            
            //create a pdf document object
            string text = "The development of the human placenta can be better" +
                " understood by comparison with other placental forms that are" +
                " encountered in mammals. The placenta is an opposition of foetal" +
                " and parental tissue for the purpose of physiological exchange.\n" +
                "There is little mixing of material and foetal blood, and for the" +
                " most purposes the two can be considered as separate.\n" +
                "The placenta can be thought of as a symbiotic parasite unique to" +
                "vmammalian.It provides for exchange of gases, food and waste also" +
                " facilitates the de neuo production of fuel substrates and hormones" +
                " and filters potentially toxic substances.\n" +
                " Placenta have two(2) distinct separate compartments: the foetal side" +
                " consisting of the triphoblast and chrionic villi and maternal side" +
                " consisting of the deciduas basalis.";

            PdfDocument pdf = new PdfDocument();

            //create an empty page
            PdfPage pdfPage = pdf.AddPage();

            //create an XGraphics object
            XGraphics graph = XGraphics.FromPdfPage(pdfPage);

            //create font objecct from XFont
            XFont font = new XFont("Times New Roman", 10, XFontStyle.Regular);
            XTextFormatter tf = new XTextFormatter(graph);

            XRect rect = new XRect(10, 10, pdfPage.Width-20, pdfPage.Height-20);
            tf.Alignment = XParagraphAlignment.Justify;
            tf.DrawString(text, font, XBrushes.Black, rect, XStringFormats.TopLeft);

            // Save the document...
            pdf.Save(filename);
            // ...and start a viewer.
            Process.Start(filename);
        }
    }
}
